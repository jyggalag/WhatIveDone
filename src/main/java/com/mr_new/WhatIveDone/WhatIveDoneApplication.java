package com.mr_new.WhatIveDone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WhatIveDoneApplication {

	public static void main(String[] args) {
		SpringApplication.run(WhatIveDoneApplication.class, args);
	}

}

